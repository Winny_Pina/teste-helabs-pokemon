
angular.module('PokedexApp', ['ngRoute', 'firebase'] )

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		controller:'MainController',
		templateUrl:'views/main.html'
	}).when('/number/:dexnum', {
		controller:'DexController',
		templateUrl:'views/pokemon.html'
	}).otherwise({
		redirectTo: '/'
	})

}])
.controller('MainController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
	var url ="http://pokeapi.co/api/v1/pokedex/1/";
	$http.get(url).success(function(data){
		$scope.totaldex = data;
		console.log($scope.totaldex.pokemon)
		$scope.allpokemon = $scope.totaldex.pokemon.filter(function(pokemon, index) {
			var num = parseInt(pokemon.resource_uri.slice(15, -1));
			return num < 10000;
		});
		for(key in $scope.allpokemon) {
			if($scope.allpokemon.hasOwnProperty(key)) {
				$scope.allpokemon[key].number = parseInt($scope.allpokemon[key].resource_uri.slice(15, -1));
			}

	}
		console.log($scope.allpokemon)
	})

}])
.controller('DexController', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http) {
	$scope.dexnum = $routeParams.dexnum;
	$scope.root = "http://pokeapi.co";
	var url = "http://pokeapi.co/api/v1/pokemon/" + $scope.dexnum + "/";
		$http.get(url).success(function(data) {
		$scope.pokemon = data;
		$scope.prior = $scope.pokemon.national_id-1;
		$scope.next= $scope.pokemon.national_id+1
		$scope.getData($scope.pokemon.sprites[0].resource_uri);
		$scope.getData($scope.pokemon.descriptions[0].resource_uri);
	});

	$scope.getData = function(uri) {
		var url = $scope.root + uri;
		$http.get(url).success(function(data) {
			$scope.uriData = data;
			if($scope.uriData.hasOwnProperty("image")) {
				$scope.pokeSprite = $scope.root + $scope.uriData.image; 
			} if($scope.uriData.hasOwnProperty("description")) {
				$scope.pokeDes =$scope.uriData.description;
			}
		})
	}

}])

.controller('AddPostCtrl', ['$scope', 
	function($scope) {
    $scope.success = false;
    var firebaseObj = new Firebase("https://dazzling-torch-8806.firebaseio.com/Comments/");
   
	    $scope.AddPost = function(){
			var name = $scope.post.name;
		    var email = $scope.post.email;
		    var comment =  $scope.post.comment;
			var id = $scope.pokemon.name;	
				
			var usersRef = firebaseObj.child(id);
				usersRef.push().set({		  
				    name: name,
				    email: email,
				    comment: comment		 
				});
			 $scope.post = {
		    	'name'     : '',
		    	'email'      : '',
		    	'comment'	:''
			};
			$scope.success = true;
	    }
}])
.controller('showCommentsCtrl', function($scope, $http) {
  $http.get("https://dazzling-torch-8806.firebaseio.com/Comments/"+$scope.pokemon.name+".json")
  .success(function (response) {$scope.names = response});
});

